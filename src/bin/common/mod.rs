extern crate getopts;
use getopts::Options;

pub fn get_version_info(name: &str, version: &'static str) -> String {
    return format!(
        "{} Version {}
Copyright (C) 2019\nLicensed under the BSD 3-clause license \
(https://gitlab.com/brb3/crustutils/blob/master/LICENSE)
",
        name, version
    );
}

pub fn print_usage(program: &str, usage: &str, opts: Options) {
    let brief = format!("Usage: {} {}", program, usage);
    print!("{}", opts.usage(&brief));
}
