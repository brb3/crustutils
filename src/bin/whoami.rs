extern crate getopts;
mod common;
use getopts::Options;
use std::env;
use std::process::exit;
use pwd::Passwd;

const VERSION: &str = "0.1.0";
const USAGE: &str = "[OPTION]]\nPrint the user name assosciated with the current user ID";
const NAME: &str = "whoami";

fn main() {
    let args: Vec<String> = env::args().collect();
    let mut opts = Options::new();

    opts.optflag("h", "help", "print this help menu");
    opts.optflag("v", "version", "print the program version");

    let matches = match opts.parse(&args[1..]) {
        Ok(m) => m,
        Err(f) => {
            println!("Error parsing arguments: {}", f.to_string());
            exit(1)
        }
    };
    if matches.opt_present("h") {
        common::print_usage(&NAME, &USAGE, opts);
        return;
    }
    if matches.opt_present("version") {
        print!("{}", common::get_version_info(&NAME, &VERSION));
        return;
    }

    match Passwd::current_user() {
        Some(u) => {
            println!("{}", u.name);
        },
        None => {
            println!("Cannot find current user");
            exit(1)
        }
    };
}