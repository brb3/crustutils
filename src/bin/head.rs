extern crate getopts;
mod common;
use getopts::Options;
use std::convert::TryInto;
use std::env;
use std::fs::File;
use std::io::{BufRead, BufReader, Read};
use std::process::exit;

const VERSION: &str = "0.1.0";
const USAGE: &str = "[OPTIONS] [FILE]\nPrint the first 10 lines of each FILE to STDOUT";
const NAME: &str = "head";

fn main() {
    let args: Vec<String> = env::args().collect();
    let mut opts = Options::new();

    /* TODO
    opts.optopt(
        "c",
        "bytes",
        "print the first NUM bytes of each file, or all but the last NUM bytes if NUM is negative",
        "NUM",
    );
    */
    opts.optopt(
        "n",
        "lines",
        "print the first NUM lines of each file, or all but the last NUM lines if NUM is negative",
        "NUM",
    );
    opts.optflag("q", "quiet", "do not print the filename headers");
    /* TODO
    opts.optflag("z", "zero-terminated", "use NUL as line delimeter");
    */
    opts.optflag("v", "verbose", "always print the filename headers");
    opts.optflag("h", "help", "print this help menu");
    opts.optflag("", "version", "print the program version");

    let mut bytes = 0;
    let mut lines = 10;
    let mut headers = true;
    let mut headers_always = false;
    let mut zero_terminated = false;

    let matches = match opts.parse(&args[1..]) {
        Ok(m) => m,
        Err(f) => {
            println!("Error parsing arguments: {}", f.to_string());
            exit(1)
        }
    };
    if matches.opt_present("h") {
        common::print_usage(&NAME, &USAGE, opts);
        return;
    }
    if matches.opt_present("version") {
        print!("{}", common::get_version_info(&NAME, &VERSION));
        return;
    }
    if matches.opt_present("c") {
        match matches.opt_str("c") {
            // TODO Handle unparseable option
            Some(b) => bytes = b.parse::<i32>().unwrap(),
            None => {}
        };
    }
    if matches.opt_present("n") {
        match matches.opt_str("n") {
            // TODO Handle unparseable option
            Some(l) => lines = l.parse::<i32>().unwrap(),
            None => {}
        };
    }
    if matches.opt_present("q") {
        headers = false
    }
    if matches.opt_present("v") {
        headers_always = true
    }
    if matches.opt_present("z") {
        zero_terminated = true
    }

    if headers && matches.free.len() == 1 && headers_always == false {
        headers = false;
    }

    generate_output(matches.free, bytes, lines, headers, zero_terminated)
}

fn generate_output(
    files: Vec<String>,
    bytes: i32,
    lines: i32,
    headers: bool,
    zero_terminated: bool,
) {
    for filename in files {
        let file = match File::open(&filename) {
            Ok(f) => f,
            Err(e) => {
                println!("cannot open '{}' for reading: {}", filename, e);
                continue;
            }
        };

        if headers {
            println!("==> {} <==", filename)
        }

        if bytes > 0 {
            print_bytes(bytes, file, zero_terminated)
        } else {
            print_lines(lines, file, zero_terminated)
        }
    }
}

fn print_bytes(_bytes: i32, _file: File, _zero: bool) {
    // TODO
}

fn print_lines(mut lines: i32, mut file: File, _zero: bool) {
    // TODO Support NUL as line terminator

    if lines >= 0 {
        let mut buf_reader = BufReader::new(file);
        while lines > 0 {
            let mut line = String::new();
            let bytes_read = match buf_reader.read_line(&mut line) {
                Ok(b) => b,
                Err(_e) => break,
            };
            if bytes_read == 0 {
                break;
            }
            print!("{}", line);
            lines = lines - 1;
        }
        println!();
    } else {
        lines = lines * -1;
        let mut buffer = String::new();
        file.read_to_string(&mut buffer).unwrap();
        let mut head: Vec<&str> = buffer.split('\n').collect();

        // TODO Handle usize conversion failure
        let length: usize = lines.try_into().unwrap();
        head.truncate(head.len() - length);

        for l in head {
            println!("{}", l);
        }
    }
}
