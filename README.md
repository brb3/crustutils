# crustutils

`coreutils` project in Rust

## Compiling
- Install Rust (& cargo)
    - Install `rustup` from your OS package manager
    - Use `rustup toolchain install stable` to install the latest stable version of rust
    - Use `rustup default stable` to set the default rust toolchain to `stable`
- Run `cargo build --release`
- Binaries from `src/bin/*.rs` will be in `target/release`

## Progress

|   TODO    | Incomplete|    Done   |
|-----------|-----------|-----------|
| arch      | head      | false     |
| base64    |           | true      |
| basename  |           | whoami    |
| cat       |           |           |
| chcon     |           |           |
| chgrp     |           |           |
| chmod     |           |           |
| chown     |           |           |
| chroot    |           |           |
| cksum     |           |           |
| comm      |           |           |
| cp        |           |           |
| csplit    |           |           |
| cut       |           |           |
| date      |           |           |
| dd        |           |           |
| df        |           |           |
| dir       |           |           |
| dircolors |           |           |
| dirname   |           |           |
| du        |           |           |
| echo      |           |           |
| env       |           |           |
| expand    |           |           |
| expr      |           |           |
| factor    |           |           |
| fmt       |           |           |
| fold      |           |           |
| groups    |           |           |
| hostid    |           |           |
| hostname  |           |           |
| id        |           |           |
| install   |           |           |
| join      |           |           |
| kill      |           |           |
| link      |           |           |
| ln        |           |           |
| logname   |           |           |
| ls        |           |           |
| md5sum    |           |           |
| mkdir     |           |           |
| mkfifo    |           |           |
| mknod     |           |           |
| mktemp    |           |           |
| mv        |           |           |
| nice      |           |           |
| nl        |           |           |
| nohup     |           |           |
| nproc     |           |           |
| numfmt    |           |           |
| od        |           |           |
| paste     |           |           |
| pathchk   |           |           |
| pinky     |           |           |
| pr        |           |           |
| printenv  |           |           |
| printf    |           |           |
| ptx       |           |           |
| pwd       |           |           |
| readlink  |           |           |
| realpath  |           |           |
| rm        |           |           |
| rmdir     |           |           |
| runcon    |           |           |
| seq       |           |           |
| shred     |           |           |
| shuf      |           |           |
| sleep     |           |           |
| sort      |           |           |
| split     |           |           |
| stat      |           |           |
| stdbuf    |           |           |
| stty      |           |           |
| sum       |           |           |
| tac       |           |           |
| tail      |           |           |
| tee       |           |           |
| test      |           |           |
| timeout   |           |           |
| touch     |           |           |
| tr        |           |           |
| truncate  |           |           |
| tsort     |           |           |
| tty       |           |           |
| uname     |           |           |
| unexpand  |           |           |
| uniq      |           |           |
| unlink    |           |           |
| uptime    |           |           |
| users     |           |           |
| vdir      |           |           |
| wc        |           |           |
| who       |           |           |
| yes       |           |           |